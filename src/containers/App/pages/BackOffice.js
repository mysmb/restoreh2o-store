import React, { Component } from 'react';

import { Button, Modal } from 'react-bootstrap';

import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { selectSlug, selectAuth} from '../selectors';
import { addToCart } from '../actions';


class BackOfficeRaw extends Component {
  render() {
    let {first_name, last_name, slug} = this.props.auth.account
    return (      
      <Modal.Dialog style={{top: 100}}>
        <Modal.Header>
          Welcome, {first_name} {last_name}!
        </Modal.Header>
        <Modal.Body>
          <div >
            <p>
              Welcome to your Restore H2O Back Office! Over the coming weeks, we'll be releasing more features here,
              so please check back often.
            </p>
            <p>
              Recruit others to your team at {' '}
                <a href={`https://join.restoreh2o.com?sponsor=${slug}`} target="_blank">https://join.restoreh2o.com?sponsor={slug}</a>. 
                <br/>
              Customers can purchase from you at {' '}
                <a href={`https://www.restoreh2o.com/store/${slug}/home`} target="_blank">https://www.restoreh2o.com/store/{slug}/home</a>              
            </p> 
          </div>
        </Modal.Body>
      </Modal.Dialog>
    );
  }
}

const mapStateToProps = (state) => ({
  slug: selectSlug(state),
  auth: selectAuth(state),
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ addToCart }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(BackOfficeRaw);
