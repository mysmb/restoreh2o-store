import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Link } from 'react-router-dom'

import FontAwesome from 'react-fontawesome'

import {selectCart, selectSlug} from './selectors'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import {formatPrice} from '../../utils/Formatters'


import { OverlayTrigger, Overlay, Popover, Button, Modal} from 'react-bootstrap'

import ReactTimeout from 'react-timeout'

class Cart extends Component {
  state = {
    showModal: false,
  }

  render(){
    let {cart, slug} = this.props
    let {items, subtotal} = cart
    
    let amount = subtotal

    const children = items.map( (i, idx) => 
      <PopoverItem key={idx} item={i} 
        onMouseEnter={ () =>  this.onMouseEnter() } 
        onMouseOut={(e) => this.onMouseOut('item', e) }/>
    )

    return (      
      <div> 
        <a ref="target" className="cart-link"
              onClick={() => this.togglePopover() }
              onMouseEnter={() => this.onMouseEnter() }               
              onMouseOut={() => this.onMouseOut('target') }>
          <FontAwesome name="shopping-cart"/> &nbsp;
          <span className="label">
            Your cart: {items.length} {items.length == 1 ? 'item' : 'items'} – {formatPrice(amount)}          
          </span>
          {!this.state.showModal && <FontAwesome name="caret-down" className="cart-action"/>}
          {this.state.showModal && <FontAwesome name="times" className="cart-action"/>}
        </a>
      
        <Overlay
          show={ (this.state.showModal || this.state.modalToggled) && items.length > 0}
          onHide={() => this.setState({ ...this.state, showModal: false })}
          placement="bottom"
          container={document.body}
          target={() => ReactDOM.findDOMNode(this.refs.target)}
          positionTop="100px"
        >
            <Popover id="cart-popover" className="cart-popover border-rounded">
              <div> 
                {children}
              </div>
              <div className="row subtotal">
                <div className="col-sm-6">Subtotal:</div>
                <div className="col-sm-6 text-right">{formatPrice(amount)}</div>
              </div>
              <div className="row actions">
                <div className="col-sm-12 align-center">
                  <Link className="btn btn-info white-border" to={`/store/${slug}/checkout`} onClick={ () => this.closePopover()}>View Cart / Checkout</Link>                  
                </div>                
              </div>
            </Popover>
        </Overlay>        
      </div>
    )
  }

  togglePopover() {
    this.setState({
      ...this.state, modalToggled: !this.state.modalToggled
    })
  }

  closePopover() {
    this.setState({
      ...this.state, modalToggled: false, showModal: false
    })
  }

  onMouseEnter() {
    let timeoutId = this.cancelCloseTimer()
    this.setState({
      ...this.state, timeoutId, showModal: true
    })    
  }

  cancelCloseTimer() {
    let timeoutId = this.state.timeoutId
    if( timeoutId ){
      this.props.clearTimeout(timeoutId)
      timeoutId = undefined
    }
    return timeoutId
  }

  resetCloseTimer() {
    const timeoutId = this.props.setTimeout(() => {
      this.setState({
        ...this.state,
        timeoutId: undefined,
        showModal: false
      })    
    }, 3000)
    

    this.setState({
      ...this.state, timeoutId
    })
  }

  onMouseOut(arg, e ) {
    window.event = e 
    this.resetCloseTimer()
  }

  showCheckout() {

  }

}

class PopoverItem extends Component {

  render(){
    let {item} = this.props

    let imageStyle = {
      background: `#efefef url(${item.image}) center center`,
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
    }
    return (
      <div className="row cart-item" onMouseEnter={this.props.onMouseEnter} onMouseOut={this.props.onMouseOut}>
        <div className="col-sm-3"> 
          <div className="image" style={imageStyle}></div>
        </div>
        <div className="col-sm-9"> 
          <div className="name">
            {item.name}
          </div>
          <div className="details">
            {item.quantity} x {formatPrice(item.price)}
          </div> 
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  cart: selectCart(state).toJS(),
  slug: selectSlug(state)
});

const mapDispatchToProps = (dispatch) => ({
  // actions: bindActionCreators({ addToCart }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ReactTimeout(Cart));