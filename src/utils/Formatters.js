

export const formatPrice = (price) => {
  if( ! price ){
    return '$0.00'
  }
  return (price / 100).toLocaleString('en-US', { style: 'currency', currency: 'USD' });
}