Created from https://github.com/YUzhva/create-react-redux-app


# Improved folder Structure
```
- src
  - components // reusable react components without redux
    * ComponentName
      tests
      index.js // entry point for component

  - containers // react components with redux and redux-saga data fetching
    * ContainerName
      tests
      index.js // entry point for container
      constants/actions/reducer/sagas/selectors.js // place container required files in root

  - global-reducer.js // connect other containers reducers here
  - global-sagas.js // connect other containers sagas here
  ```